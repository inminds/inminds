<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

	function __construct() {
		parent::__construct();
        $this->load->library('utilitarios');
	}

    public function index()
    {
        $contacto = $this->utilitarios->getTPLtoString('contacto');
        $trabajos = $this->utilitarios->getTPLtoString('trabajos');
        $equipo = $this->utilitarios->getTPLtoString('equipo');
        $quehacemos = $this->utilitarios->getTPLtoString('quehacemos');
        $testimonios = $this->utilitarios->getTPLtoString('testimonios');
        $porque = $this->utilitarios->getTPLtoString('porque');
        $datos = array(
            'QUEHACEMOS' => $quehacemos,
            'PORQUE' => $porque,
            'EQUIPO' => $equipo,
            'CONTACTO' => $contacto,
            'CLIENTES' => "",
            'TRABAJOS' => "",
            'TESTIMONIOS' => "");

        $this->utilitarios->show('main',$datos);
    }

    public function privacidad() {
        $this->utilitarios->show('privacidad');
    }

}

?>
