<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller{

	function __construct() {
		parent::__construct();
        $this->load->library('utilitarios');
    }

    public function index(){
        $this->utilitarios->show('contacto');
    }

    public function email()
    {  
        $nombre = $this->input->post('nombre');
        $email = $this->input->post('email');
        $telefono = $this->input->post('telefono');
        $consulta = $this->input->post('consulta');

        $email_resp = "inminds.tuc@gmail.com";
        $asunto = 'Contacto desde la web';
        $destino = "Inminds";

        $this->email->from('contacto@inminds.com.ar', 'Inminds');
        $this->email->to($email_resp);
        $this->email->bcc("
            bautista.luis.88@gmail.com,
            santuchocarolina@gmail.com,
            nachocerimele@gmail.com,
            javier.caniparoli@gmail.com
        ");
        $this->email->subject($asunto);
        $datos = array(
            'asunto' => $asunto,
            'nombre' => $nombre,
            'email' => $email,
            'telefono' => $telefono,
            'consulta' => $consulta,
            'destino' => $destino
        );
        $this->email->message($this->load->view('emails/email', $datos, true));
        if ($this->email->send()){
           $data['status'] = 'OK';
        }
        else{
          $data['status'] = 'error';
        }
        echo json_encode($data);
    }
}