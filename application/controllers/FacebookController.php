<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class FacebookController extends CI_Controller {

    var $app_id ='905972636100554';//'905972636100554';
    var $app_secret = '560e32e06b68085ad9d53e4421a4b9dd';
    var $version = 'v2.3';

    public function __construct() {

        parent::__construct();
        $this->load->database();
        $this->load->library('FacebookSDK');
    }

    public function index() {

        $fb = new Facebook\Facebook(array(
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => $this->version,
        ));

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = 'token-que-no-vence';
            $accessToken = $helper->getAccessToken();
            $linkData = [
                'link' => 'http://javiercani.github.io/',
                'message' => "Probando",
            ];
            var_dump($fb->post('/feed', $linkData, $accessToken));
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
        }
    }

}
 ?>