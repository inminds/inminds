<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resource extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    private function loadArray($dataView = array())
    {
        $dataView['BASE'] = base_url();
        $dataView['CSS_VENDOR'] = base_url() . "Resource/css_vendor/";
        $dataView['CSS'] = base_url() . "Resource/css/";
        $dataView['JS_VENDOR'] = base_url() . "Resource/js_vendor/";
        $dataView['JS'] = base_url() . "Resource/js/";
        $dataView['IMG'] = base_url()."Resource/img/";

        require APPPATH.'config/database.php';
        $dataView['URLsocket'] = 'http://'.$db['default']['hostname'].':999';

        return $dataView;

    }

    private function loadImage($file_path, $mime_type_or_return = 'image/png')
    {
        header("Content-type: ".$mime_type_or_return);
        header("Content-length: ".filesize(getcwd().$file_path));
        header("Content-Disposition: inline; filename=".getcwd().$file_path);
        readfile(getcwd().$file_path);
    }

    public function index(){}

    public function css($cssName)
    {
        header("Content-type: text/css");
        $this->parser->parse('css/'.$cssName.'.css',$this->loadArray());
    }

    public function css_vendor($cssName)
    {
        header("Content-type: text/css");
        $this->parser->parse('css/vendor/'.$cssName.'.css',$this->loadArray());
    }

    public function js($jsName)
    {
        header("Content-Type: application/javascript");
        $this->parser->parse('js/'.$jsName.'.js',$this->loadArray());
    }

    public function js_vendor($jsName)
    {
        header("Content-Type: application/javascript");
        $this->parser->parse('js/vendor/'.$jsName.'.js',$this->loadArray());
    }

    public function img($imgName)
    {
        $this->loadImage('/application/views/img/'.$imgName);
    }

}

?>