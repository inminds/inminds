<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenerateClass extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('ORM/Reflect');
        $this->load->database();
    }

    /**
     * generate entity objects automatically from mysql db tables
     * @return none
     */
    function index()
    {
        $tables = $this->db->query('show tables');

        $r = new Reflect();

        //Generando entidades ------------------
        echo "Generando Clases...<br><br>";
        foreach($tables->result_array() as $t)
        {
            $r->createClass($t['Tables_in_'.$this->db->database]);
            echo $t['Tables_in_'.$this->db->database].' Generada...<br>';

        }

        // Generando Relaciones ------------------
        echo "<br><br>Generando Relaciones...<br><br>";
        $Relations = json_decode(file_get_contents(getcwd()."/application/models/ORM/relation.json"));

        if(count($Relations)>0)
        {
            foreach($Relations as $rel)
            {
                if($rel->type=='1')
                {
                    $vEntities = explode('=',$rel->relation);
                    $e1 = $vEntities[0];
                    $e2 = $vEntities[1];

                    $vDataEnt1 = explode('.',$e1);
                    $vDataEnt2 = explode('.',$e2);

                    echo $rel->relation."<br>";
                    $r->createRelation($vDataEnt1[0],$vDataEnt1[1],$vDataEnt2[0],$vDataEnt2[1],$rel->type);
                    $r->createRelation($vDataEnt2[0],$vDataEnt2[1],$vDataEnt1[0],$vDataEnt1[1],$rel->type);
                }
            }
        }


    }

}
?>