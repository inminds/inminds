<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Logo" content="http://www.inminds.com.ar/assets/images/logo.png">
    <meta name="Author" lang="es" content="Inminds Tucuman Argentina">
    <meta property="og:title" content="InMinds - Desarrollo de software"/>
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="InMinds"/>
    <meta property="og:url" content="http://www.inminds.com.ar/"/>
    <meta property="og:description" content="InMinds - Desarrollo de software"/>
    <meta property="og:image" content="http://www.inminds.com.ar/assets/images/logo.png"/>
    <meta name="author" content="InMinds">
    <meta name="keywords" content="inminds,desarrollo,web,mobile,marketing,tucuman,responsive"/>
    <meta name="Description" content="Visitá nuestro sitio para informarte sobre Desarrollos y Marketing"/>

    <title>Minds</title>

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Main stylesheet -->
    <link href="assets/css/hallooou.css" rel="stylesheet">

    <!-- Color stylesheet -->
    <link href="assets/css/colors.css" rel="stylesheet">


    <!-- Plugin stylesheets -->
    <link href="assets/css/plugins/owl.carousel.css" rel="stylesheet">
    <link href="assets/css/plugins/owl.theme.css" rel="stylesheet">
    <link href="assets/css/plugins/owl.transitions.css" rel="stylesheet">
    <link href="assets/css/plugins/animate.css" rel="stylesheet">
    <link href="assets/css/plugins/magnific-popup.css" rel="stylesheet">
    <link href="assets/css/plugins/jquery.mb.YTPlayer.min.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <script>
        document.createElement('video');
      </script>
    <![endif]-->

    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

</head>

<body id="home">
    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header pull-left">
                <a class="navbar-brand page-scroll" href="#page-top">
                    <!-- replace with your brand logo/text -->
                    <span class="brand-logo"><img style="height:35px; margin-top: -8px;" src="assets/images/logo1.png" onerror="this.src='assets/images/logo.png'; this.onerror=null;" alt="InMinds" title="InMinds" class="img-responsive"></span>
                </a>
            </div>
            <div class="main-nav pull-right">
                <div class="button_container toggle">
                    <span class="top"></span>
                    <span class="middle"></span>
                    <span class="bottom"></span>
                </div>
            </div>
            <div class="overlay" id="overlay">
                <nav class="overlay-menu">
                    <ul>
                        <li><a href="#services">Que Hacemos</a></li>
                        <li><a href="#porqueelegirnos">¿Por qué Elegirnos?</a></li>
                        <li><a href="#team">El equipo</a></li>
                        <li><a href="#contact">Contactenos</a></li>
                    </ul>
                </nav>
            </div>
        </div><!-- /.container -->
    </nav>

{VIEW}

 <!-- Footer -->
    <footer>
        <div class="container">

            <div class="row text-center">
                <div class="col-md-12 social segment">
                    <h4>SEGUINOS</h4>
                    <a href="https://web.facebook.com/inminds.tuc"><i class="fa fa-facebook fa-3x"></i></a>
                    <!--<a href="#"><i class="fa fa-twitter fa-3x"></i></a>-->
                    <a href="https://www.linkedin.com/company-beta/21280520"><i class="fa fa-linkedin fa-3x"></i></a>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->

        </div><!-- /.container -->

        <div class="nav pull-right scroll-top">
            <a href="#home" title="Scroll to top"><i class="fa fa-angle-up"></i></a>
        </div>

    </footer><!-- /.footer -->
</body>

<!-- Plugin JavaScript -->
<script src="assets/js/plugins/wow.min.js"></script>
<script src="assets/js/plugins/owl.carousel.min.js"></script>
<script src="assets/js/plugins/jquery.parallax-1.1.3.js"></script>
<script src="assets/js/plugins/jquery.magnific-popup.min.js"></script>
<script src="assets/js/plugins/jquery.mb.YTPlayer.min.js"></script>
<script src="assets/js/plugins/jquery.countTo.js"></script>
<script src="assets/js/plugins/jquery.inview.min.js"></script>
<script src="assets/js/plugins/pace.min.js"></script>
<script src="assets/js/plugins/jquery.easing.min.js"></script>
<script src="assets/js/plugins/jquery.validate.min.js"></script>
<script src="assets/js/plugins/additional-methods.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoVKfEihX__NdMwdDysA6Vve6PE9Ierj4"></script>-->

<!-- Scripts Inminds -->

<!-- Custom JavaScript -->
<script src="assets/js/hallooou.js"></script>

<!-- Código de instalación CLIENGO para www.inminds.com.ar --> 
<script>(function(){var ldk=document.createElement('script'); ldk.type='text/javascript'; ldk.async=true; ldk.src='https://s.cliengo.com/weboptimizer/598daf47e4b0bb94536178c6/598daf4be4b0bb94536178ce.js' ; var s=document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ldk, s);})();
</script>

<!-- Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-104628143-1', 'auto');
  ga('send', 'pageview');
</script>

</html>
