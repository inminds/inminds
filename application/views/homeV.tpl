
            <!-- Intro Header -->
            <!-- Full Page Image Background Carousel Header -->
            <header id="intro-carousel" class="carousel slide">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item html5-video-container active">
                        <!-- HTML5 Video -->
                        <video muted autoplay loop poster="assets/images/typing-on-mac.jpg" id="html5-video" class="fill">
                            <source src="assets/videos/typing-on-mac.webm" type="video/webm">
                                <source src="assets/videos/typing-on-mac.mp4" type="video/mp4">
                        </video>
                        <div class="carousel-caption">
                            <h1 class="animated slideInDown">Servicios de Ingenieria y Diseño</h1>
                            <p class="intro-text animated slideInUp">Destaque frente a la competencia</p>
                            <a href="http://bit.ly/hh5_template" class="btn btn-default btn-lg">Conozcanos</a>
                        </div>
                        <div class="html5-video-controls">
                            <a id="html5-video-volume" class="fa fa-volume-up fa-lg" href="#"></a>
                            <a id="html5-video-play" class="fa fa-pause fa-lg" href="#"></a>
                        </div>
                        <div class="overlay-detail"></div>
                    </div><!-- /.item -->

                </div><!-- /.carousel-inner -->
                <div class="mouse"></div>
            </header>



            <!-- Services Section -->
            <section id="services" class="services content-section">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2>Que hacimo</h2>
                            <h3 class="caption gray">Nuestros principales servicios</h3>
                        </div><!-- /.col-md-12 -->

                        <div class="container">
                            <div class="row text-center">
                                <div class="col-md-4">
                                    <div class="row services-item sans-shadow text-center">
                                        <i class="fa fa-cogs fa-3x"></i>
                                        <h4>Desarrollo</h4>
                                        <p>Construimos soluciones a medida que resuelvan algunos problemas.</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="row services-item sans-shadow text-center">
                                        <i class="fa fa-paint-brush fa-3x"></i>
                                        <h4>Interfaces</h4>
                                        <p>Nos pone cachondos hacer diseños atractivas y funcionales</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="row services-item sans-shadow text-center">
                                        <i class="fa fa-bullhorn fa-3x"></i>
                                        <h4>Marketing Social</h4>
                                        <p>Lo haremos vender tanto humo que llevara su negocio a un nivel superior</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="row services-item sans-shadow text-center">
                                        <i class="fa fa-database fa-3x"></i>
                                        <h4>Diseño de Sistemas</h4>
                                        <p>ECada sistema necesita un entorno diferente donde lo hace mas exitoso</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-4 -->


                                <div class="col-md-4">
                                    <div class="row services-item sans-shadow text-center">
                                        <i class="fa fa-camera fa-3x"></i>
                                        <h4></h4>
                                        <p>Specializing in product and photo journalistic style photography</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-4 -->

                            </div><!-- /.row -->
                        </div><!-- /.container -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.section -->



            <!-- Products Section -->
            <section id="products" class="products content-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="assets/images/hallooou_template-iphone_white.png" class="center-block img-responsive">
                        </div><!-- /.col-md-4 -->

                        <div class="col-md-8">
                            <div class="products-container">

                                <div class="col-md-12">
                                    <h2>¿Porque somos exitosos?</h2>
                                    <h3 class="caption white">Brindamos todo lo que los clientes quieren</h3>
                                </div><!-- /.col-md-12 -->

                                <div class="col-md-6 product-item">
                                    <div class="media-left">
                                        <span class="icon"><i class="fa fa-arrows-alt fa-3x"></i></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Estructura Responsive</h3>
                                        <p>Funcionalidades que se adaptan a la medida del dispositivo.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-6 product-item">
                                    <div class="media-left">
                                        <span class="icon"><i class="fa fa-eye fa-3x"></i></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Retina Ready Graphics</h3>
                                        <p>Looks beautiful &amp; ultra-sharp on Retina Displays with Retina Icons, Fonts &amp; Images.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-6 product-item">
                                    <div class="media-left">
                                        <span class="icon"><i class="fa fa-arrows-v fa-3x"></i></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Parallax Sections</h3>
                                        <p>Comes with Parallax effect script so you can add Parallax effect to any section of the website.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-6 product-item">
                                    <div class="media-left">
                                        <span class="icon"><i class="fa fa-video-camera fa-3x"></i></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">YouTube &amp; HTML5 Background Video</h3>
                                        <p>Choose to display either YouTube or HTML5 as background video.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-6 product-item">
                                    <div class="media-left">
                                        <span class="icon"><i class="fa fa-toggle-on fa-3x"></i></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Color Options</h3>
                                        <p>Choose a color that suits your brand &amp; change the color of the template with just one CSS file.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-6 product-item">
                                    <div class="media-left">
                                        <span class="icon"><i class="fa fa-envelope-o fa-3x"></i></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Contact form</h3>
                                        <p>Fully functional PHP contact form, with user input validation.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                            </div><!-- /.products-container -->
                        </div><!-- /.col-md-8 -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.products -->



            <!-- Our team Section -->
            <section id="team" class="team content-section">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2>Our Team</h2>
                            <h3 class="caption gray">Meet the people who make awesome stuffs</h3>
                        </div><!-- /.col-md-12 -->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/lauren-cox.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae asperiores mollitia.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Lauren Cox</h4>
                                        <p>Creative Director</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/jessie-barnett.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Neque minima ea, a praesentium saepe nihil maxime quod esse numquam explicabo eligendi.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Jessie Barnett</h4>
                                        <p>UI/UX Designer</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/terry-fletcher.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Temporibus dolor, quisquam consectetur molestias, veniam voluptatum. Beatae alias omnis totam.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Terry Fletcher</h4>
                                        <p>Web Developer</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->


                                 <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/lauren-cox.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae asperiores mollitia.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Lauren Cox</h4>
                                        <p>Creative Director</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/jessie-barnett.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Neque minima ea, a praesentium saepe nihil maxime quod esse numquam explicabo eligendi.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Jessie Barnett</h4>
                                        <p>UI/UX Designer</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/terry-fletcher.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Temporibus dolor, quisquam consectetur molestias, veniam voluptatum. Beatae alias omnis totam.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Terry Fletcher</h4>
                                        <p>Web Developer</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->

                            </div><!-- /.row -->
                        </div><!-- /.container -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.our-team -->



            <!-- Portfolio/Gallery Section -->
            <section id="portfolio" class="portfolio content-section parallax">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2>Some of our recent work</h2>
                            <h3 class="caption white">Designed with passion and coded to perfection, Hallooou is a stylish responsive HTML template, that is packed with tons of features yet is so easy to use. </h3>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->

                <div class="container project-container text-center">
                    <div class="recent-project-carousel owl-carousel owl-theme popup-gallery">
                        <div class="item recent-project">
                            <img src="assets/images/gallery/project-one.jpg" alt="">
                            <div class="project-info">
                                <h3>Lakeview Inc.</h3>
                                <ul class="project-meta">
                                    <li><a href="#">Website Design</a></li>
                                </ul>
                            </div><!-- /.project-info -->
                            <div class="full-project">
                                <a href="assets/images/gallery/project-one_xl.jpg" title="Lakeview Inc. | Website Design">View project <i class="fa fa-chevron-right"></i></a>
                            </div><!-- /.full-project -->
                        </div><!-- /.item -->

                        <div class="item recent-project">
                            <img src="assets/images/gallery/project-two.jpg" alt="">
                            <div class="project-info">
                                <h3>Adam Productions</h3>
                                <ul class="project-meta">
                                    <li><a href="#">App Design</a></li>
                                </ul>
                            </div><!-- /.project-info -->
                            <div class="full-project">
                                <a href="assets/images/gallery/project-two_xl.jpg" title="Adam Productions | App Design">View project <i class="fa fa-chevron-right"></i></a>
                            </div><!-- /.full-project -->
                        </div><!-- /.item -->

                        <div class="item recent-project">
                            <img src="assets/images/gallery/project-three.jpg" alt="">
                            <div class="project-info">
                                <h3>Doe Associates</h3>
                                <ul class="project-meta">
                                    <li><a href="#">Branding</a></li>
                                </ul>
                            </div><!-- /.project-info -->
                            <div class="full-project">
                                <a href="assets/images/gallery/project-three_xl.jpg" title="Doe Associates | Branding">View project <i class="fa fa-chevron-right"></i></a>
                            </div><!-- /.full-project -->
                        </div><!-- /.item -->

                        <div class="item recent-project">
                            <img src="assets/images/gallery/project-four.jpg" alt="">
                            <div class="project-info">
                                <h3>Omega Corporation</h3>
                                <ul class="project-meta">
                                    <li><a href="#">App Design</a></li>
                                </ul>
                            </div><!-- /.project-info -->
                            <div class="full-project">
                                <a href="assets/images/gallery/project-four_xl.jpg" title="Omega Corporation | App Design">View project <i class="fa fa-chevron-right"></i></a>
                            </div><!-- /.full-project -->
                        </div><!-- /.item -->

                    </div><!-- /.recent-project-carousel -->

                    <div class="customNavigation project-navigation text-center">
                        <a class="btn-prev"><i class="fa fa-angle-left fa-2x"></i></a>
                        <a class="btn-next"><i class="fa fa-angle-right fa-2x"></i></a>
                    </div><!-- /.project-navigation -->

                </div><!-- /.container -->
            </section><!-- /.portfolio -->



            <!-- Our clients -->
            <section id="clients" class="our-clients content-section text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Our clients</h2>
                            <h3 class="caption white">Decent listing with your clients. Fully customizable and ready to use in any subpages as you want. Responsive for any numbers of clients in this section.</h3>
                        </div><!-- /.col-md-12-->
                    </div><!-- /.row -->

                    <div class="row client-slider">
                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/sony-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/nike-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/paramount-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/microsoft-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/citibank-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/fedex-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/sony-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/nike-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->
                        
                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/paramount-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/microsoft-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/citibank-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->
                        
                        <div class="item col-xs-4 col-md-2 i">
                            <a href="#" title="#">
                                <img src="assets/images/fedex-logo.png" class="img-responsive">
                            </a>
                        </div><!-- /.col-xs-4 -->

                    </div><!-- /.row -->

                </div><!-- /.container -->
            </section><!-- /.our-clients -->



            <!-- Testimonials Section -->
            <section class="testimonials content-section">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2>Rated 4.8 out of 5</h2>
                            <h3 class="caption gray">Here's what our clients have to say</h3>
                        </div><!-- /.col-md-12 -->
                        
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="client-testimonials owl-carousel owl-theme">

                                        <div class="item">
                                            <p class="speech">Hallooou is the best HTML5 template we have EVER purchased. It's the perfect solution for our business. The unlimited customization helps us get things done quickly and efficiently.</p>
                                            <div class="client-info">
                                                <img src="assets/images/gina_stone-client_pic.jpg" />
                                                <h4>Gina Stone</h4>
                                                <span>Adam Productions</span>
                                            </div>
                                        </div><!-- /.item -->

                                        <div class="item">
                                            <p class="speech">This is the real deal! Very easy to use. Thanks to Hallooou HTML5 template, we've just launched our new website! I couldn't have asked for more than this. Definitely worth the investment.</p>
                                            <div class="client-info">
                                                <img src="assets/images/john_doe-client_pic.jpg" />
                                                <h4>John Doe</h4>
                                                <span>Doe Associates</span>
                                            </div>
                                        </div><!-- /.item -->

                                        <div class="item">
                                            <p class="speech">We're loving it. The best on the net! I strongly recommend Hallooou HTML5 template to everyone interested in running a successful business! Hallooou HTML5 template has really helped our business.</p>
                                            <div class="client-info">
                                                <img src="assets/images/dana_hill-client_pic.jpg" />
                                                <h4>Dana Hill</h4>
                                                <span>Lakeview Inc.</span>
                                            </div>
                                        </div><!-- /.item -->

                                    </div><!-- /.client-testimonials -->
                                </div><!-- /.col-md-8 -->

                            </div><!-- /.row -->
                        </div><!-- /.container -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /#testimonials -->



            <!-- Call to action - one section -->
            <section class="cta-one-section content-section alt-bg-light">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-6">
                            <h2>InMinds es una empresa destinada</h2>
                            <h3 class="caption gray">Optional title for call to action</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto quis quam, culpa deserunt hic et numquam eius accusamus expedita quidem sit voluptate!</p>

                            <a href="#" class="btn btn-default btn-lg">Questions?</a>
                            <a href="#" class="btn btn-outlined btn-lg">Contact us</a>
                        </div><!-- /.col-md-6 -->

                        <div class="col-md-6">
                            <img src="assets/images/hallooou-responsive.png" class="img-responsive">
                        </div><!-- /.col-md-6 -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.cta-one-section -->



            <!-- Counter Section -->
            <section id="counter" class="counter-section content-section">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2 class="white">Our Achievements</h2>
                            <h3 class="caption">Ultricestum urna litora sit sed praesent condimentum eu a et scelerisque</h3>
                        </div><!-- /.col-md-12 -->

                        <div class="col-sm-3 counter-wrap">
                            <strong><span class="timer">350</span>+</strong>
                            <span class="count-description">Clients Worked With</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="col-sm-3 counter-wrap">
                            <strong><span class="timer">250</span>+</strong>
                            <span class="count-description">Nominations</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="col-sm-3 counter-wrap">
                            <strong><span class="timer">160</span>+</strong>
                            <span class="count-description">Awards Won</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="col-sm-3 counter-wrap">
                            <strong><span class="timer">1650</span>+</strong>
                            <span class="count-description">Cups Of Coffee</span>
                        </div><!-- /.col-sm-3 -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.counter-section -->



            <!-- Call to action - two section -->
            <section class="cta-two-section">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <h3>Have an idea?</h3>
                            <p>We’re here to help you manage your work</p>
                        </div>
                        <div class="col-sm-3">
                            <a href="#" class="btn btn-overcolor">Get in touch</a>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.cta-two-section -->
            
           