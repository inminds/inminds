<html>
	<body>
		<h1>Politica de Privacidad de nuestra App</h1>
		<p>
			Nosotros InMinds Argentina en Facebook respetamos su privacidad y valoramos la relación que tenemos con usted. El uso de esta aplicación está sujeta a esta Política de Privacidad. El uso de Facebook en general y su relación con Facebook están sujetas a la propia política de privacidad y otros términos de Facebook y políticas que InMinds Argentina no controla. Esta política de privacidad describe que InMinds Argentina puede utilizar la información que obtenemos con esta aplicación en Facebook y con quién podemos compartirla.</p>
<h3>La información que recopilamos</h3>
<p>Con esta solicitud, podemos recoger dos tipos de información personal:</p>
<ul>
	<ol>
		Información que recibimos de Facebook.- La información y el contenido que recibimos de Facebook es proporcionada por usted u otros usuarios de Facebook o se asoció por Facebook con un usuario en particular.
	</ol>
	<ol>
		Información Independiente.- La información y el contenido que se provea directamente a nosotros a través de un formulario en esta aplicación.		
	</ol>
</ul>
<h3>La Información que recibimos de Facebook</h3>
<p>
Cuando usted visite la aplicación, se puede recoger toda la información que es visible para "Todos" en Facebook, así como la información de Facebook designa la información a disposición del público. Esto incluye su nombre, foto de perfil, sexo y perfil público. Incluso si usted no ha permitido este acceso a las aplicaciones, se puede recopilar información que recibimos de Facebook acerca de usted, cuando sus amigos en Facebook permiten el acceso y uso de la aplicación, sin perjuicio de los ajustes de Facebook. Utilizamos esta información para operar la aplicación, colocar la publicación en tu muro o compartir con otros usuarios. Dicha información se publicará en su muro o será compartida con otros usuarios sin su aprobación explícita.</p>

<h3>Información Independiente</h3>
<p>
La aplicación puede pedirle que proporcione cierta información de forma explícita a nosotros, como su dirección de correo electrónico de una lista predefinida para enviar a tus amigos. Por lo general, utilizamos la información independiente poder realizar un registro y poder mantenerlo al tanto de nuevas actualizaciones o de algún cambio que tenga la aplicación. Como también nos podemos dirigir a usted para notificarle algún premio por su participación en la aplicación.</p>
<h3>¿Cómo comunicarse con nosotros?</h3>
<p>
Si posee alguna pregunta o comentario acerca de este aviso de privacidad, por favor contáctese con nosotros por correo electrónico a inminds.tuc@gmail.com</p>
</body>
</html>