
            <!-- Counter Section
            <section id="counter" class="counter-section content-section">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2 class="white">Our Achievements</h2>
                            <h3 class="caption">Ultricestum urna litora sit sed praesent condimentum eu a et scelerisque</h3>
                        </div>

                        <div class="col-sm-3 counter-wrap">
                            <strong><span class="timer">350</span>+</strong>
                            <span class="count-description">Clients Worked With</span>
                        </div>

                        <div class="col-sm-3 counter-wrap">
                            <strong><span class="timer">250</span>+</strong>
                            <span class="count-description">Nominations</span>
                        </div>

                        <div class="col-sm-3 counter-wrap">
                            <strong><span class="timer">160</span>+</strong>
                            <span class="count-description">Awards Won</span>
                        </div>

                        <div class="col-sm-3 counter-wrap">
                            <strong><span class="timer">1650</span>+</strong>
                            <span class="count-description">Cups Of Coffee</span>
                        </div>

                    </div>
                </div>
            </section> /.counter-section -->

        <!-- Contact section -->
            <section id="contact" class="contact content-section no-bottom-pad">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2>Contacto</h2>
                            <h3 class="caption gray"><b>Ponte en contacto con nosotros si tienes en mente un nuevo proyecto o simplemente alguna idea asombrosa.</b></h3>
                        </div><!-- /.col-md-12 -->

                    </div><!-- /.row -->
                </div><!-- /.container -->

                <div class="container">
                    <div class="row form-container">

                        <div class="col-md-8 contact-form">
                            <h3>Dejanos tu mensaje</h3>
                            <form class="ajax-form" id="contactForm" method="post" role="form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Tu nombre..." value="" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Tu email..." value="" required>
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="telefono" name="telefono" placeholder="Tu telefono..." value="" required>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="4" id="consulta" name="consulta" placeholder="Tu mensaje..."></textarea>
                                </div>
                                <div id="mensaje">La consulta no puede estar vacía. </div><br/>
                                <div class="form-group">
                                    <input name="btn_contact" id="btn_contact" class="btn btn-default" type="submit" value="Enviar">
                                </div>
                            </form>
                        </div><!-- /.contact-form -->

                        <div class="col-md-4 contact-address">
                            <h3>Nuestra ubicación</h3>
                            <p>
                                <br />San Miguel de Tucumán, Tucumán.
                                <br /> Argentina</p>
                            <ul>
                                <li><span>Email:</span>inminds.tuc@gmail.com</li>
                                <li><span>Telefono:</span>+54 9 381 301-9676</li>
                            </ul>
                            <br><br>
                            <h6>¿Te gustó este sitio? Compartelo.</h6>
                            <div class="col s12">
                                <a href="https://twitter.com/share" class="twitter-share-button" data-size="large" data-hashtags="inminds">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                <iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.inminds.com.ar%2Findex.html&layout=button&size=large&mobile_iframe=true&appId=301237167028926&width=99&height=28" width="99" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                            </div><hr>
                        </div><!-- /.contact-address -->
                        
                        
                    </div><!-- /.row -->
                </div><!-- /.container -->

                <div class="container-fluid buffer-forty-top hidden">
                    <div class="row">
                        <section id="cd-google-map no-bottom-pad">
                            <div id="google-container"></div>
                            <div id="cd-zoom-in"></div>
                            <div id="cd-zoom-out"></div>
                        </section>
                    </div>
                </div>



<div class="modal fade" id="modalContact">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="gridSystemModalLabel" ><b>Mensaje</b></h5>
            </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>  
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script src="{JS}contacto"></script>
