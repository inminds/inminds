<!-- Products Section -->
            <section id="porqueelegirnos" class="products content-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="assets/images/hallooou_template-iphone_white.png" class="center-block img-responsive">
                        </div><!-- /.col-md-4 -->

                        <div class="col-md-8">
                            <div class="products-container">

                                <div class="col-md-12">
                                    <h2>¿Por qué elegirnos?</h2>
                                    <h3 class="caption white">Porque brindamos lo que el cliente necesita basado en estos 4 valores.</h3>
                                </div><!-- /.col-md-12 -->

                                <div class="col-md-12 product-item">
                                    <div class="media-left">
                                        <span class="icon"><img src="assets/images/icono_experiencia.png" style="width: 100%"></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Experiencia</h3>
                                        <p>Nuestra experiencia tanto individual como en equipo, nos permite encarar proyectos de cualquier envergadura.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-12 product-item">
                                    <div class="media-left">
                                        <span class="icon"><img src="assets/images/icono_responsabilidad.png" style="width: 40%; margin-top: -4px;"></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Responsabilidad</h3>
                                        <p>Entendemos que la responsabilidad en todo lo que hacemos es fundamental para tener éxito en cada desafio.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-12 product-item">
                                    <div class="media-left">
                                        <span class="icon"><img src="assets/images/icono_compromiso.png" style="width: 140%; margin-left: -7px;"></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Compromiso</h3>
                                        <p>Cada proyecto lo desarrollamos con pasión, dandole la importancia que merece, como si fuese propio.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-12 product-item">
                                    <div class="media-left">
                                        <span class="icon"><img src="assets/images/icono_puntualidad.png" style="width: 70%"></span>
                                    </div><!-- /.media-left -->
                                    <div class="media-body">
                                        <h3 class="media-heading">Puntualidad</h3>
                                        <p>Sabemos que el tiempo es muy importante para nuestro cliente, por eso la puntualidad en la entrega es prioridad para nosotros.</p>
                                    </div><!-- /.media-body -->
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.products-container -->
                        </div><!-- /.col-md-8 -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.products -->