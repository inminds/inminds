
            <!-- Intro Header -->
            <!-- Full Page Image Background Carousel Header -->
            <header id="intro-carousel" class="carousel slide">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item html5-video-container active">
                        <!-- HTML5 Video -->
                        <video muted autoplay loop poster="assets/images/typing-on-mac.jpg" id="html5-video" class="fill">
                            <source src="assets/videos/typing-on-mac.webm" type="video/webm">
                                <source src="assets/videos/typing-on-mac.mp4" type="video/mp4">
                        </video>
                        <div class="carousel-caption">
                            <h1 class="animated slideInDown">¡Apasionados por lo que hacemos!</h1>
                            <p class="intro-text animated slideInUp">Expertos en desarrollo web y marketing digital</p>
                            <a href="#services" id="btn_conozcanos" class="btn btn-default btn-lg">Conozcanos</a><br>
                            <a href="#contact" id="btn_presupuesto" class="btn btn-default btn-lg" style="margin-top:15px;">Solicite un presupuesto</a>
                        </div>
                        <div class="html5-video-controls">
                            <a id="html5-video-volume" class="fa fa-volume-up fa-lg" href="#"></a>
                            <a id="html5-video-play" class="fa fa-pause fa-lg" href="#"></a>
                        </div>
                        <div class="overlay-detail"></div>
                    </div><!-- /.item -->

                </div><!-- /.carousel-inner -->
                <div class="mouse"></div>
            </header>


{QUEHACEMOS}
{PORQUE}
{EQUIPO}
{TRABAJOS}
{CLIENTES}
{TESTIMONIOS}
{CONTACTO}
