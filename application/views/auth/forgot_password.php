<link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css'?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url().'assets/css/font-awesome.css' ?>">
<link rel="stylesheet" href="<?php echo base_url().'Resource/css/app'?>" rel="stylesheet">
<br>
<br>
<body class="fondo">
  <div class="container">
    <?php if(isset($message) && $message!=FALSE && $message!=''):?>
      <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong><?php echo $message?></strong>
      </div>
    <?php endif?>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
          <div class="panel panel-default panel-login">
            <div class="panel-body">
              <h5 class="text-center letra-login">RECUPERAR CONTRASEÑA</h5>
              <form class="form form-signup letra-campos" role="form" action="<?php echo base_url('auth/forgot_password')?>" method="post">
                <div class="form-group">
                  <div >
                    <label class="control-label col-sm-2" style="font-size: 15px;color: #008137;margin-top: 7px;" for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="identity" placeholder="Ingrese su E-mail">
                    </div>
                  </div>
                </div>
                <br>
                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-md btn-primary btn-block btn-login" role="button" value="Enviar"/> 
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
  <div>
</body>