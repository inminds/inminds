<link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css'?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url().'assets/css/font-awesome.css' ?>">
<link rel="stylesheet" href="<?php echo base_url().'Resource/css/app'?>" rel="stylesheet">
<br>
<br>
<body class="fondo">
  <div class="container">
   <?php if(isset($message) && $message!=FALSE && $message!=''):?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong><?php echo $message?></strong>
    </div>
    <?php endif?>
    <div class="row">
      <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
        <div class="panel panel-default panel-login">
          <div class="panel-body">
            <h5 class="text-center letra-login">ACCESO A EXTRANET</h5>
            <form class="form form-signup letra-campos" role="form" action="<?php echo base_url('auth/login')?>" method="post">
              <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                    <input type="text" class="form-control" placeholder="Usuario" id="identity" name="identity"/>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                    <input type="password" class="form-control" placeholder="Contraseña" id="password" name="password"/>
                </div>
              </div>
              <div class="form-group">
                  <div class="input-group">
                      <label class="checkbox-inline" style="font-size: 15px;color: #008137;"><input id="remember" type="checkbox" value=""> Recordarme</label>
                  </div>
                  <input type="submit" name="submit" class="btn btn-md btn-primary btn-block btn-login" role="button" value="Ingresar"/> 
              </div>
             
            </form>
            <p><a href="forgot_password" style="font-size: 15px;color: #008137;"><?php echo lang('login_forgot_password');?></a></p>
          </div>
        </div>
      </div>
    </div>
  <div>
</body>