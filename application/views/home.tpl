<link href="{CSS}Home" rel="stylesheet">
<div ng-controller="controllerHome">
    <div class="row paddOff marginOff">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 marginOff paddOff">

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 marginOff paddOff">
            <div class="carritos">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a data-toggle="pill" href="#carroPrincipal">Principal</a>
                    </li>
                    <li role="presentation">
                        <a data-toggle="pill" href="#menu1">Carrito 2</a>
                    </li>
                    <li role="presentation">
                        <a data-toggle="pill" href="#menu1">Carrito 2</a>
                    </li>
                    <li role="presentation">
                        <a data-toggle="pill" href="#nuevoCarro">
                            <span class="glyphicon glyphicon-plus-sign"></span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="carroPrincipal" class="tab-pane fade in active carro">
                        <h3>HOME</h3>
                        <p>Some content.</p>
                    </div>
                    <div id="menu1" class="tab-pane fade carro">
                        <h3>Menu 1</h3>
                        <p>Some content in menu 1.</p>
                    </div>
                    <div id="nuevoCarro" class="tab-pane fade carro">
                        <h3>Nuevo Carrito</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{URLsocket}/socket.io/socket.io.js"></script>
<script src="{JS}carritos"></script>
<script src="{JS}home"></script>