            <!-- Services Section -->
            <section id="services" class="services content-section">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2>Que hacemos</h2>
                            <h3 class="caption gray"><b><spam class="a red">Hacemos que nuestros clientes digan ¡WOW!</spam> Nos divierte y moviliza hacer lo que nos apasiona, y saber que cada trabajo es un logro y una satisfaccion reflejada en las palabras de quienes confiaron en nuestro trabajo.</b></h3>
                        </div><!-- /.col-md-12 -->

                        <div class="container">
                            <div class="row text-center">
                                <div class="col-md-3">
                                    <div class="row services-item sans-shadow text-center">
                                        <i class="fa fa-globe fa-3x"></i>
                                        <h4>Desarrollo y diseño web</h4>
                                        <p>Nos enfocamos en el usuario de cada sitio, esto nos permite crear interfaces funcionales y amigables.</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-3 -->

                                <div class="col-md-3">
                                    <div class="row services-item sans-shadow text-center">
                                        <i class="fa fa-bullhorn fa-3x"></i>
                                        <h4>Marketing digital</h4>
                                        <p>Nos encargamos del posicionamiento de su empresa en buscadores</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-3 -->

                                <div class="col-md-3">
                                    <div class="row services-item sans-shadow text-center">
                                        <div class="divAppMovil">
                                            <img src="assets/images/icono_app.png" class="center-block img-responsive imgAppMovil">    
                                        </div>
                                        <h4>Aplicaciones Moviles</h4>
                                        <p>Creamos aplicaciones móviles que innovan y posicionan su negocio</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-3 -->

                                <div class="col-md-3">
                                    <div class="row services-item sans-shadow text-center">
                                        <i class="fa fa-shopping-cart fa-3x"></i>
                                        <h4>E-Commerce</h4>
                                        <p>Brindamos soluciones y soporte en plataformas de comercio electronico</p>
                                    </div><!-- /.row -->
                                </div><!-- /.col-md-3 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.section -->
