            <!-- Our team Section -->
            <section id="team" class="team content-section">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2>El equipo</h2>
                        </div><!-- /.col-md-12 -->

                        <div class="container">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/lucho.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae asperiores mollitia.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Luis Bautista</h4>
                                        <p>Systems Engineer</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/cani.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Neque minima ea, a praesentium saepe nihil maxime quod esse numquam explicabo eligendi.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Javier Caniparoli</h4>
                                        <p>Systems Engineer</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="team-member">
                                        <figure>
                                            <img src="assets/images/nacho.jpg" alt="" class="img-responsive">
                                            <figcaption>
                                                <p>Temporibus dolor, quisquam consectetur molestias, veniam voluptatum. Beatae alias omnis totam.</p>
                                                <ul>
                                                    <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                                                    <li><a href=""><i class="fa fa-linkedin fa-2x"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <h4>Ignacio Cerimele</h4>
                                        <p>Graphic Designer</p>
                                    </div><!-- /.team-member -->
                                </div><!-- /.col-md-4 -->                            

                            </div><!-- /.row -->
                        </div><!-- /.container -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.our-team -->
