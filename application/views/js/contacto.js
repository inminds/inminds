$('#mensaje').hide();

var enviar_email = function () {
    sending_mail(true);
	var data = {
	    nombre:   $('#nombre').val(),
	    email:   $('#email').val(),
	    telefono: $('#telefono').val(),
	    consulta:   $('#consulta').val()
	}

	$.post('Contacto/email', data, function(resp){
        sending_mail(false);
	    if(resp.status == 'OK'){
	        $('#mensaje').hide();
	        $('#consulta').val('');
	        $(".modal-body").html("<span>Su mensaje ha sido enviado correctamente.<br>Nos comunicaremos con Ud. a la brevedad.<br>Gracias</span>");
	        $('#modalContact').modal('show');
	    }
	    else{
	        if(resp.status == 'consulta'){
	            $('#mensaje').show();
	        }
	        else{
	            $('#mensaje').hide();
	            $(".modal-body").html("<span style='color:red;'>Ha Ocurrido un problema en el envio de su mensaje.<br>Por favor intente mas tarde o comuníquese telefónicamente.<br>Gracias y disculpe las molestias.</span>");
	            $('#modalContact').modal('show');
	        }
	    }
	},'json');
};

var sending_mail = function(status){
    if(status){
        $("#btn_contact").val("Enviando...");
    }
    else{
        $("#btn_contact").val("Enviar");
    }
};
