<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class {Tabla} extends CI_Model
{
    {atributos}

	public function __construct($id=null)
    {
        parent::__construct();

        if(!is_null($id))
        {
            $result = $this->db->query("SELECT * FROM {tabla} WHERE {AtributoPK}=".$id.";");
            if(count($result->result_array())>0)
            {
                $res = $result->result_array();
                foreach ($res[0] as $campo => $valor )
                {
                    $this->$campo = $valor;
                }
            }
        }
        else
        {
            $this->{AtributoPK} = null;
        }
    }

        {seters}

        {geters}

    public function save()
    {
        if($this->{AtributoPK}==null)
        {
           $insertArray = array(
                {INSERT_ATRIB}
            );
            $this->db->insert('{tabla}',$insertArray);
            $this->{AtributoPK} = $this->db->insert_id();

        }
        else
        {
           $updateArray = array(
                {UPDATE}
            );
            $this->db->update('{tabla}', $updateArray, array('{AtributoPK}' => $this->{AtributoPK}));
            
        }
    }

    public function delete()
    {
        $this->db->delete('{tabla}', array('{AtributoPK}' => $this->{AtributoPK})); 
    }

    public function show()
    {
        return "
                    <div class='panel panel-default'>
                      <div class='panel-body'>
                        {AttribShow}
                      </div>
                    </div>
                    ";
    }

    public function convertToJSON()
    {
        $var = get_object_vars($this);
        return json_encode($var);
    }

    public function setAttributesJSON($JSON)
    {
        $Obj = json_decode($JSON);
        $result = "";
        foreach($Obj as $Attribute => $val)
        {
            if(property_exists($this,$Attribute))
            {
                $this->$Attribute = $val;
            }
        }
    }

    //---|||SimplePHP|||---//

}

// - Coleccion

Class Collection{Tabla} extends CI_Model
{
    private $count;
    public ${tabla};

    //$condicion es un array
    public function __construct($condicion=null,$limit=null, $principio=null,$orderCol=null,$order=null)
    {   
         parent::__construct();

        if($orderCol==null){$orderCol = "{AtributoPK}";$order="ASC";}
        $this->db->order_by($orderCol, $order);
        $result = $this->db->get_where('{tabla}', $condicion, $limit, $principio);
        $this->count = count($result->result_array());
        if($this->count>0)
        {
            $this->{tabla} = array();
            for ($i=0;$i<$this->count;$i++)
            {
                $ob = new {Tabla}(null);
                $res = $result->result_array();
                foreach ($res[$i] as $campo => $valor )
                {
                    $MCampo = 'set'.ucwords($campo);
                    $ob->$MCampo($valor);
                }
                array_push($this->{tabla},$ob);
            }
        }
    }

    public function save()
    {
        for($i=0;$i<$this->count;$i++)
        {
            $this->{tabla}[$i]->save();
        }
    }

    public function convertToJSON()
    {
        $arrJson = array();
        for($i=0;$i<count($this->{tabla});$i++)
        {
            array_push($arrJson,$this->{tabla}[$i]->convertToJSON());
        }
        return "[".implode(",",$arrJson)."]";
    }

    public function count()
    {
        return $this->count;
    }

}
?>