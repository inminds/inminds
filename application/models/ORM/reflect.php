<?php

class Reflect extends CI_Model
{
    private $RAIZ;
    public function __construct()
    {
        parent::__construct();
        $this->RAIZ = getcwd();
    }

	public function createClass($tableName)
    {
		$dir = $this->RAIZ."/application/models/ORM/Entities/";
		$maquetaClass = file_get_contents($this->RAIZ."/application/models/ORM/class.tpl.php");
		$clase = $tableName;

		//********************************************************************

        if(!is_dir($dir))
        {
            mkdir($dir);
        }

		$atrib = $this->db->query("DESCRIBE ".$clase);

        if($this->db->count_all_results()>0)
        {
            //Clases
            $defAtrib = "";
            $PK_AutoIncremental = "";
            $seters = "";
            $geters = "";
            $insertAtrib = "";
            $insertValues = "";
            $updateValues = "";
            $attribShow = "";

            foreach($atrib->result_array() AS $row) {

                //------- Definicion de Atributos
                $defAtrib = $defAtrib."private $".$row['Field'].";
    ";
                //------- Atributo PK AutoIncremental
                if($row['Extra']=='auto_increment')
                {
                    $PK_AutoIncremental = $row['Field'];
                }
                // -------- Seters
                    $seters = $seters."public function set".ucwords($row['Field'])."($".$row['Field'].")
		        {
		            \$this->".$row['Field']." = $".$row['Field'].";
		        }
		        ";
                // ------- Geters
                $geters = $geters."public function get".ucwords($row['Field'])."()
		        {
		        return \$this->".$row['Field'].";
		        }
		        ";
                //------- Atributo para Insert
                if($row['Extra']!='auto_increment')
                {
                    ($insertAtrib=="")?($insertAtrib = "'".$row['Field']."'=>\$this->".$row['Field']):($insertAtrib = $insertAtrib.",
                '".$row['Field']."'=>\$this->".$row['Field']);

                }

                //------- Valores para Insert
                if($row['Extra']!='auto_increment')
                {
                    if(strpos($row['Type'],'int') || strpos($row['Type'],'tinyint') || strpos($row['Type'],'smallint') || strpos($row['Type'],'mediumint') || strpos($row['Type'],'bigint') || strpos($row['Type'],'float') || strpos($row['Type'],'decimal'))
                    {
                        ($insertValues=="")?($insertValues = "\$this->".$row['Field']):($insertValues = $insertValues.",\$this->".$row['Field']);
                    }
                    else
                    {
                        ($insertValues=="")?($insertValues = "'\$this->".$row['Field']."'"):($insertValues = $insertValues.",'\$this->".$row['Field']."'");
                    }

                }

                //-------------- valores para UPDATE
                if($row['Extra']!='auto_increment')
                {
                    ($updateValues=="")?($updateValues = "'".$row['Field']."'=>\$this->".$row['Field']):($updateValues = $updateValues.",'".$row['Field']."'=>\$this->".$row['Field']);

                }

                //---------------- atributos para Show
                $attribShow .= "
                            <b>".$row['Field'].": </b>\".\$this->".$row['Field'].".\"<br>";

            }

            $contenido = $this->parse($maquetaClass, array(
                    "Tabla" => ucwords($clase),
                    "tabla" => $clase,
                    "atributos" => $defAtrib,
                    "AtributoPK" => $PK_AutoIncremental,
                    "seters" => $seters,
                    "geters" => $geters,
                    "INSERT_ATRIB" => $insertAtrib,
                    "INSERT_VALUES" => $insertValues,
                    "UPDATE" => $updateValues,
                    "AttribShow" => $attribShow
                )
            );
            $file=fopen($dir."/".ucwords($clase).".php","w");
            fwrite($file,$contenido);
            fclose($file);

            return true;
        }
        else
        {
            return false;
        }

	}

    private function parse($view, $data)
    {
        foreach ($data as $key => $value)
        {
            $view = str_replace('{'.$key.'}', $value, $view);
        }
        return ($view);
    }

    public function createRelation($clase,$clase_id,$claseRelacion,$claseRelacion_id,$tipoRelacion)
    {
        $dir = $this->RAIZ."/application/models/ORM/Entities";
        $maquetaClass = file_get_contents($dir."/".$clase.".php");

        //********************************************************************

        $methodo = "";
        if($tipoRelacion=="1")
        {
            $methodo = "

        public function get".ucwords($claseRelacion)."()
        {
            \$o".ucwords($claseRelacion)." = new ".ucwords($claseRelacion)."(\$this->".$clase_id.");
            return \$o".ucwords($claseRelacion).";
        }

        public function getCollection".ucwords($claseRelacion)."(\$wh = array())
        {
            \$wh ['".$claseRelacion_id."']= (\$this->".$clase_id.");
            
            \$o".ucwords($claseRelacion)." = new Collection".ucwords($claseRelacion)."(\$wh);
            return \$o".ucwords($claseRelacion).";
        }

        //---|||SimplePHP|||---//

        ";
        }
        else
        {

        }

        $maquetaClass = str_replace("//---|||SimplePHP|||---//",$methodo,$maquetaClass);

        $ruta = $dir."/".ucwords($clase).".php";
        $contenido = $maquetaClass;
        unlink($ruta);
        $file=fopen($ruta,"w");
        fwrite($file,$contenido);
        fclose($file);
    }


}

 ?>