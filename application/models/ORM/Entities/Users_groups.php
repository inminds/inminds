<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Users_groups extends CI_Model
{
    private $id;
    private $user_id;
    private $group_id;
    

	public function __construct($id=null)
    {
        parent::__construct();

        if(!is_null($id))
        {
            $result = $this->db->query("SELECT * FROM users_groups WHERE id=".$id.";");
            if(count($result->result_array())>0)
            {
                $res = $result->result_array();
                foreach ($res[0] as $campo => $valor )
                {
                    $this->$campo = $valor;
                }
            }
        }
        else
        {
            $this->id = null;
        }
    }

        public function setId($id)
		        {
		            $this->id = $id;
		        }
		        public function setUser_id($user_id)
		        {
		            $this->user_id = $user_id;
		        }
		        public function setGroup_id($group_id)
		        {
		            $this->group_id = $group_id;
		        }
		        

        public function getId()
		        {
		        return $this->id;
		        }
		        public function getUser_id()
		        {
		        return $this->user_id;
		        }
		        public function getGroup_id()
		        {
		        return $this->group_id;
		        }
		        

    public function save()
    {
        if($this->id==null)
        {
           $insertArray = array(
                'user_id'=>$this->user_id,
                'group_id'=>$this->group_id
            );
            $this->db->insert('users_groups',$insertArray);
            $this->id = $this->db->insert_id();

        }
        else
        {
           $updateArray = array(
                'user_id'=>$this->user_id,'group_id'=>$this->group_id
            );
            $this->db->update('users_groups', $updateArray, array('id' => $this->id));
            
        }
    }

    public function delete()
    {
        $this->db->delete('users_groups', array('id' => $this->id)); 
    }

    public function show()
    {
        return "
                    <div class='panel panel-default'>
                      <div class='panel-body'>
                        
                            <b>id: </b>".$this->id."<br>
                            <b>user_id: </b>".$this->user_id."<br>
                            <b>group_id: </b>".$this->group_id."<br>
                      </div>
                    </div>
                    ";
    }

    public function convertToJSON()
    {
        $var = get_object_vars($this);
        return json_encode($var);
    }

    public function setAttributesJSON($JSON)
    {
        $Obj = json_decode($JSON);
        $result = "";
        foreach($Obj as $Attribute => $val)
        {
            if(property_exists($this,$Attribute))
            {
                $this->$Attribute = $val;
            }
        }
    }

    

        public function getUsers()
        {
            $oUsers = new Users($this->user_id);
            return $oUsers;
        }

        public function getCollectionUsers($wh = array())
        {
            $wh ['id']= ($this->user_id);
            
            $oUsers = new CollectionUsers($wh);
            return $oUsers;
        }

        

        public function getGroups()
        {
            $oGroups = new Groups($this->group_id);
            return $oGroups;
        }

        public function getCollectionGroups($wh = array())
        {
            $wh ['id']= ($this->group_id);
            
            $oGroups = new CollectionGroups($wh);
            return $oGroups;
        }

        //---|||SimplePHP|||---//

        

        

}

// - Coleccion

Class CollectionUsers_groups extends CI_Model
{
    private $count;
    public $users_groups;

    //$condicion es un array
    public function __construct($condicion=null,$limit=null, $principio=null,$orderCol=null,$order=null)
    {   
         parent::__construct();

        if($orderCol==null){$orderCol = "id";$order="ASC";}
        $this->db->order_by($orderCol, $order);
        $result = $this->db->get_where('users_groups', $condicion, $limit, $principio);
        $this->count = count($result->result_array());
        if($this->count>0)
        {
            $this->users_groups = array();
            for ($i=0;$i<$this->count;$i++)
            {
                $ob = new Users_groups(null);
                $res = $result->result_array();
                foreach ($res[$i] as $campo => $valor )
                {
                    $MCampo = 'set'.ucwords($campo);
                    $ob->$MCampo($valor);
                }
                array_push($this->users_groups,$ob);
            }
        }
    }

    public function save()
    {
        for($i=0;$i<$this->count;$i++)
        {
            $this->users_groups[$i]->save();
        }
    }

    public function convertToJSON()
    {
        $arrJson = array();
        for($i=0;$i<count($this->users_groups);$i++)
        {
            array_push($arrJson,$this->users_groups[$i]->convertToJSON());
        }
        return "[".implode(",",$arrJson)."]";
    }

    public function count()
    {
        return $this->count;
    }

}
?>