<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Users extends CI_Model
{
    private $id;
    private $ip_address;
    private $username;
    private $password;
    private $salt;
    private $email;
    private $activation_code;
    private $forgotten_password_code;
    private $forgotten_password_time;
    private $remember_code;
    private $created_on;
    private $last_login;
    private $active;
    private $first_name;
    private $last_name;
    private $company;
    private $phone;
    

	public function __construct($id=null)
    {
        parent::__construct();

        if(!is_null($id))
        {
            $result = $this->db->query("SELECT * FROM users WHERE id=".$id.";");
            if(count($result->result_array())>0)
            {
                $res = $result->result_array();
                foreach ($res[0] as $campo => $valor )
                {
                    $this->$campo = $valor;
                }
            }
        }
        else
        {
            $this->id = null;
        }
    }

        public function setId($id)
		        {
		            $this->id = $id;
		        }
		        public function setIp_address($ip_address)
		        {
		            $this->ip_address = $ip_address;
		        }
		        public function setUsername($username)
		        {
		            $this->username = $username;
		        }
		        public function setPassword($password)
		        {
		            $this->password = $password;
		        }
		        public function setSalt($salt)
		        {
		            $this->salt = $salt;
		        }
		        public function setEmail($email)
		        {
		            $this->email = $email;
		        }
		        public function setActivation_code($activation_code)
		        {
		            $this->activation_code = $activation_code;
		        }
		        public function setForgotten_password_code($forgotten_password_code)
		        {
		            $this->forgotten_password_code = $forgotten_password_code;
		        }
		        public function setForgotten_password_time($forgotten_password_time)
		        {
		            $this->forgotten_password_time = $forgotten_password_time;
		        }
		        public function setRemember_code($remember_code)
		        {
		            $this->remember_code = $remember_code;
		        }
		        public function setCreated_on($created_on)
		        {
		            $this->created_on = $created_on;
		        }
		        public function setLast_login($last_login)
		        {
		            $this->last_login = $last_login;
		        }
		        public function setActive($active)
		        {
		            $this->active = $active;
		        }
		        public function setFirst_name($first_name)
		        {
		            $this->first_name = $first_name;
		        }
		        public function setLast_name($last_name)
		        {
		            $this->last_name = $last_name;
		        }
		        public function setCompany($company)
		        {
		            $this->company = $company;
		        }
		        public function setPhone($phone)
		        {
		            $this->phone = $phone;
		        }
		        

        public function getId()
		        {
		        return $this->id;
		        }
		        public function getIp_address()
		        {
		        return $this->ip_address;
		        }
		        public function getUsername()
		        {
		        return $this->username;
		        }
		        public function getPassword()
		        {
		        return $this->password;
		        }
		        public function getSalt()
		        {
		        return $this->salt;
		        }
		        public function getEmail()
		        {
		        return $this->email;
		        }
		        public function getActivation_code()
		        {
		        return $this->activation_code;
		        }
		        public function getForgotten_password_code()
		        {
		        return $this->forgotten_password_code;
		        }
		        public function getForgotten_password_time()
		        {
		        return $this->forgotten_password_time;
		        }
		        public function getRemember_code()
		        {
		        return $this->remember_code;
		        }
		        public function getCreated_on()
		        {
		        return $this->created_on;
		        }
		        public function getLast_login()
		        {
		        return $this->last_login;
		        }
		        public function getActive()
		        {
		        return $this->active;
		        }
		        public function getFirst_name()
		        {
		        return $this->first_name;
		        }
		        public function getLast_name()
		        {
		        return $this->last_name;
		        }
		        public function getCompany()
		        {
		        return $this->company;
		        }
		        public function getPhone()
		        {
		        return $this->phone;
		        }
		        

    public function save()
    {
        if($this->id==null)
        {
           $insertArray = array(
                'ip_address'=>$this->ip_address,
                'username'=>$this->username,
                'password'=>$this->password,
                'salt'=>$this->salt,
                'email'=>$this->email,
                'activation_code'=>$this->activation_code,
                'forgotten_password_code'=>$this->forgotten_password_code,
                'forgotten_password_time'=>$this->forgotten_password_time,
                'remember_code'=>$this->remember_code,
                'created_on'=>$this->created_on,
                'last_login'=>$this->last_login,
                'active'=>$this->active,
                'first_name'=>$this->first_name,
                'last_name'=>$this->last_name,
                'company'=>$this->company,
                'phone'=>$this->phone
            );
            $this->db->insert('users',$insertArray);
            $this->id = $this->db->insert_id();

        }
        else
        {
           $updateArray = array(
                'ip_address'=>$this->ip_address,'username'=>$this->username,'password'=>$this->password,'salt'=>$this->salt,'email'=>$this->email,'activation_code'=>$this->activation_code,'forgotten_password_code'=>$this->forgotten_password_code,'forgotten_password_time'=>$this->forgotten_password_time,'remember_code'=>$this->remember_code,'created_on'=>$this->created_on,'last_login'=>$this->last_login,'active'=>$this->active,'first_name'=>$this->first_name,'last_name'=>$this->last_name,'company'=>$this->company,'phone'=>$this->phone
            );
            $this->db->update('users', $updateArray, array('id' => $this->id));
            
        }
    }

    public function delete()
    {
        $this->db->delete('users', array('id' => $this->id)); 
    }

    public function show()
    {
        return "
                    <div class='panel panel-default'>
                      <div class='panel-body'>
                        
                            <b>id: </b>".$this->id."<br>
                            <b>ip_address: </b>".$this->ip_address."<br>
                            <b>username: </b>".$this->username."<br>
                            <b>password: </b>".$this->password."<br>
                            <b>salt: </b>".$this->salt."<br>
                            <b>email: </b>".$this->email."<br>
                            <b>activation_code: </b>".$this->activation_code."<br>
                            <b>forgotten_password_code: </b>".$this->forgotten_password_code."<br>
                            <b>forgotten_password_time: </b>".$this->forgotten_password_time."<br>
                            <b>remember_code: </b>".$this->remember_code."<br>
                            <b>created_on: </b>".$this->created_on."<br>
                            <b>last_login: </b>".$this->last_login."<br>
                            <b>active: </b>".$this->active."<br>
                            <b>first_name: </b>".$this->first_name."<br>
                            <b>last_name: </b>".$this->last_name."<br>
                            <b>company: </b>".$this->company."<br>
                            <b>phone: </b>".$this->phone."<br>
                      </div>
                    </div>
                    ";
    }

    public function convertToJSON()
    {
        $var = get_object_vars($this);
        return json_encode($var);
    }

    public function setAttributesJSON($JSON)
    {
        $Obj = json_decode($JSON);
        $result = "";
        foreach($Obj as $Attribute => $val)
        {
            if(property_exists($this,$Attribute))
            {
                $this->$Attribute = $val;
            }
        }
    }

    

        public function getUsers_groups()
        {
            $oUsers_groups = new Users_groups($this->id);
            return $oUsers_groups;
        }

        public function getCollectionUsers_groups($wh = array())
        {
            $wh ['user_id']= ($this->id);
            
            $oUsers_groups = new CollectionUsers_groups($wh);
            return $oUsers_groups;
        }

        //---|||SimplePHP|||---//

        

}

// - Coleccion

Class CollectionUsers extends CI_Model
{
    private $count;
    public $users;

    //$condicion es un array
    public function __construct($condicion=null,$limit=null, $principio=null,$orderCol=null,$order=null)
    {   
         parent::__construct();

        if($orderCol==null){$orderCol = "id";$order="ASC";}
        $this->db->order_by($orderCol, $order);
        $result = $this->db->get_where('users', $condicion, $limit, $principio);
        $this->count = count($result->result_array());
        if($this->count>0)
        {
            $this->users = array();
            for ($i=0;$i<$this->count;$i++)
            {
                $ob = new Users(null);
                $res = $result->result_array();
                foreach ($res[$i] as $campo => $valor )
                {
                    $MCampo = 'set'.ucwords($campo);
                    $ob->$MCampo($valor);
                }
                array_push($this->users,$ob);
            }
        }
    }

    public function save()
    {
        for($i=0;$i<$this->count;$i++)
        {
            $this->users[$i]->save();
        }
    }

    public function convertToJSON()
    {
        $arrJson = array();
        for($i=0;$i<count($this->users);$i++)
        {
            array_push($arrJson,$this->users[$i]->convertToJSON());
        }
        return "[".implode(",",$arrJson)."]";
    }

    public function count()
    {
        return $this->count;
    }

}
?>