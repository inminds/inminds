<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Groups extends CI_Model
{
    private $id;
    private $name;
    private $description;
    

	public function __construct($id=null)
    {
        parent::__construct();

        if(!is_null($id))
        {
            $result = $this->db->query("SELECT * FROM groups WHERE id=".$id.";");
            if(count($result->result_array())>0)
            {
                $res = $result->result_array();
                foreach ($res[0] as $campo => $valor )
                {
                    $this->$campo = $valor;
                }
            }
        }
        else
        {
            $this->id = null;
        }
    }

        public function setId($id)
		        {
		            $this->id = $id;
		        }
		        public function setName($name)
		        {
		            $this->name = $name;
		        }
		        public function setDescription($description)
		        {
		            $this->description = $description;
		        }
		        

        public function getId()
		        {
		        return $this->id;
		        }
		        public function getName()
		        {
		        return $this->name;
		        }
		        public function getDescription()
		        {
		        return $this->description;
		        }
		        

    public function save()
    {
        if($this->id==null)
        {
           $insertArray = array(
                'name'=>$this->name,
                'description'=>$this->description
            );
            $this->db->insert('groups',$insertArray);
            $this->id = $this->db->insert_id();

        }
        else
        {
           $updateArray = array(
                'name'=>$this->name,'description'=>$this->description
            );
            $this->db->update('groups', $updateArray, array('id' => $this->id));
            
        }
    }

    public function delete()
    {
        $this->db->delete('groups', array('id' => $this->id)); 
    }

    public function show()
    {
        return "
                    <div class='panel panel-default'>
                      <div class='panel-body'>
                        
                            <b>id: </b>".$this->id."<br>
                            <b>name: </b>".$this->name."<br>
                            <b>description: </b>".$this->description."<br>
                      </div>
                    </div>
                    ";
    }

    public function convertToJSON()
    {
        $var = get_object_vars($this);
        return json_encode($var);
    }

    public function setAttributesJSON($JSON)
    {
        $Obj = json_decode($JSON);
        $result = "";
        foreach($Obj as $Attribute => $val)
        {
            if(property_exists($this,$Attribute))
            {
                $this->$Attribute = $val;
            }
        }
    }

    

        public function getUsers_groups()
        {
            $oUsers_groups = new Users_groups($this->id);
            return $oUsers_groups;
        }

        public function getCollectionUsers_groups($wh = array())
        {
            $wh ['group_id']= ($this->id);
            
            $oUsers_groups = new CollectionUsers_groups($wh);
            return $oUsers_groups;
        }

        //---|||SimplePHP|||---//

        

}

// - Coleccion

Class CollectionGroups extends CI_Model
{
    private $count;
    public $groups;

    //$condicion es un array
    public function __construct($condicion=null,$limit=null, $principio=null,$orderCol=null,$order=null)
    {   
         parent::__construct();

        if($orderCol==null){$orderCol = "id";$order="ASC";}
        $this->db->order_by($orderCol, $order);
        $result = $this->db->get_where('groups', $condicion, $limit, $principio);
        $this->count = count($result->result_array());
        if($this->count>0)
        {
            $this->groups = array();
            for ($i=0;$i<$this->count;$i++)
            {
                $ob = new Groups(null);
                $res = $result->result_array();
                foreach ($res[$i] as $campo => $valor )
                {
                    $MCampo = 'set'.ucwords($campo);
                    $ob->$MCampo($valor);
                }
                array_push($this->groups,$ob);
            }
        }
    }

    public function save()
    {
        for($i=0;$i<$this->count;$i++)
        {
            $this->groups[$i]->save();
        }
    }

    public function convertToJSON()
    {
        $arrJson = array();
        for($i=0;$i<count($this->groups);$i++)
        {
            array_push($arrJson,$this->groups[$i]->convertToJSON());
        }
        return "[".implode(",",$arrJson)."]";
    }

    public function count()
    {
        return $this->count;
    }

}
?>