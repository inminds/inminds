<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Login_attempts extends CI_Model
{
    private $id;
    private $ip_address;
    private $login;
    private $time;
    

	public function __construct($id=null)
    {
        parent::__construct();

        if(!is_null($id))
        {
            $result = $this->db->query("SELECT * FROM login_attempts WHERE id=".$id.";");
            if(count($result->result_array())>0)
            {
                $res = $result->result_array();
                foreach ($res[0] as $campo => $valor )
                {
                    $this->$campo = $valor;
                }
            }
        }
        else
        {
            $this->id = null;
        }
    }

        public function setId($id)
		        {
		            $this->id = $id;
		        }
		        public function setIp_address($ip_address)
		        {
		            $this->ip_address = $ip_address;
		        }
		        public function setLogin($login)
		        {
		            $this->login = $login;
		        }
		        public function setTime($time)
		        {
		            $this->time = $time;
		        }
		        

        public function getId()
		        {
		        return $this->id;
		        }
		        public function getIp_address()
		        {
		        return $this->ip_address;
		        }
		        public function getLogin()
		        {
		        return $this->login;
		        }
		        public function getTime()
		        {
		        return $this->time;
		        }
		        

    public function save()
    {
        if($this->id==null)
        {
           $insertArray = array(
                'ip_address'=>$this->ip_address,
                'login'=>$this->login,
                'time'=>$this->time
            );
            $this->db->insert('login_attempts',$insertArray);
            $this->id = $this->db->insert_id();

        }
        else
        {
           $updateArray = array(
                'ip_address'=>$this->ip_address,'login'=>$this->login,'time'=>$this->time
            );
            $this->db->update('login_attempts', $updateArray, array('id' => $this->id));
            
        }
    }

    public function delete()
    {
        $this->db->delete('login_attempts', array('id' => $this->id)); 
    }

    public function show()
    {
        return "
                    <div class='panel panel-default'>
                      <div class='panel-body'>
                        
                            <b>id: </b>".$this->id."<br>
                            <b>ip_address: </b>".$this->ip_address."<br>
                            <b>login: </b>".$this->login."<br>
                            <b>time: </b>".$this->time."<br>
                      </div>
                    </div>
                    ";
    }

    public function convertToJSON()
    {
        $var = get_object_vars($this);
        return json_encode($var);
    }

    public function setAttributesJSON($JSON)
    {
        $Obj = json_decode($JSON);
        $result = "";
        foreach($Obj as $Attribute => $val)
        {
            if(property_exists($this,$Attribute))
            {
                $this->$Attribute = $val;
            }
        }
    }

    //---|||SimplePHP|||---//

}

// - Coleccion

Class CollectionLogin_attempts extends CI_Model
{
    private $count;
    public $login_attempts;

    //$condicion es un array
    public function __construct($condicion=null,$limit=null, $principio=null,$orderCol=null,$order=null)
    {   
         parent::__construct();

        if($orderCol==null){$orderCol = "id";$order="ASC";}
        $this->db->order_by($orderCol, $order);
        $result = $this->db->get_where('login_attempts', $condicion, $limit, $principio);
        $this->count = count($result->result_array());
        if($this->count>0)
        {
            $this->login_attempts = array();
            for ($i=0;$i<$this->count;$i++)
            {
                $ob = new Login_attempts(null);
                $res = $result->result_array();
                foreach ($res[$i] as $campo => $valor )
                {
                    $MCampo = 'set'.ucwords($campo);
                    $ob->$MCampo($valor);
                }
                array_push($this->login_attempts,$ob);
            }
        }
    }

    public function save()
    {
        for($i=0;$i<$this->count;$i++)
        {
            $this->login_attempts[$i]->save();
        }
    }

    public function convertToJSON()
    {
        $arrJson = array();
        for($i=0;$i<count($this->login_attempts);$i++)
        {
            array_push($arrJson,$this->login_attempts[$i]->convertToJSON());
        }
        return "[".implode(",",$arrJson)."]";
    }

    public function count()
    {
        return $this->count;
    }

}
?>