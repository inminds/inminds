<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conexion extends CI_Model
{
    private $db2;
    public function __construct()
    {
        $this->load->database();
    }
    public function __destruct(){}

    public function open()
    {
        $_host = $this->db->hostname;
        $_user = $this->db->username;
        $_pass = $this->db->password;
        $_db2ase = $this->db->database;
        $_port = 3306;

        $this->db2 = new mysqli($_host,$_user,$_pass,$_db2ase,$_port);
        if($this->db2->connect_errno)
        {
            echo "Fallo de Conexion a la db2 ".$this->db2->connet_error;
            exit();
        }

    }

    public function close()
    {
        $this->db2->close();
    }

    public function consult($_sql)
    {

        $result = $this->db2->query($_sql);

        $Tabla= array();
        if(!empty($result)){
            while($Fila=$result->fetch_assoc()){
                $Tabla[] = $Fila;
            }
            $result->close();
        }
        return $Tabla;

    }
    public function run($_sql)
    {
        if(!$this->db2->query($_sql))
        {
            $err = "Fallo en Ejecucion: ".$this->db2->error." SQL: ".$_sql;
            echo $err;
            exit();
        }
    }

    public function insert_id($_sql)
    {
        $idGenerado = 0;
        if(!$this->db2->query($_sql))
        {
            $err = "Fallo en Insert: ".$this->db2->error." Sql:".$_sql;
            echo $err;
            exit();
        }
        else
        {
            $idGenerado = $this->db2->insert_id;
        }
        return $idGenerado;
    }

}
?>