<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View_controller
{
    protected $ci;
    protected $base_url;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->base_url = $this->ci->config->item('base_url');
    }

    public function getURL()
    {
        return $this->base_url;
    }

    public function show($page,$data = array())
    {
        $data['base_url'] = $this->getURL();
        $view = $this->ci->parser->parse($page .'.html',$data,true);
        $data['view'] = $view;
        $this->ci->parser->parse('masterPage.html',$data);
    }

    public function css($file_name,$data = array())
    {
        $data['base_url'] = $this->getURL();
        header("Content-type: text/css");
        $this->ci->parser->parse('css/' . $file_name . '.css',$data);
    }

    public function js($file_name,$data = array())
    {
        $data['base_url'] = $this->getURL();
        header("Content-Type: application/javascript");
        $this->ci->parser->parse('js/' . $file_name . '.js',$data);
    }

    public function getImagen($path)
    {
        $path = "application/views/img/" . $path;
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }
}