<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utilitarios {
	
	protected $ci;
   
   	function __construct(){
      	$this->ci =& get_instance();
    }

   	public function show($view,$dataView = array()){
        // $userSess = $this->ci->session->userdata();
        // $user = new Users($userSess['user_id']);

        // if($user->getActive() == 1)
        // {
        //     //contro de acceso
        //     $view = $this->verificaPermisos($user,$view);

        //     $dataView['BASE'] = base_url();
        //     $dataView['CSS'] = base_url()."Resource/css/";
        //     $dataView['JS'] = base_url()."Resource/js/";
        //     $dataView['IMG'] = base_url()."Resource/img/";
        //     // $dataView['CIERRE_ZONA'] = $this->getCierreZona();
        //     // $dataView['CREDITO_DISPONIBLE'] = $this->getCreditoDisponible();
        //     // $dataView['APELLIDO'] = $user->getLast_name();
        //     // $dataView['NOMBRE'] = $user->getFirst_name();
        //     // $dataView['FARMACIA'] = $user->getCompany();
        //     //item de acceso al panel
        //     if($user->getGroup_id()==1)
        //     {
        //         $dataView['CONSOLA_ADMIN'] = '<li><a href="'.base_url().'panel"><i class="fa fa-terminal" aria-hidden="true"></i> Consola Admin</a></li>';
        //     }
        //     else
        //     {
        //         $dataView['CONSOLA_ADMIN'] = "";
        //     }


        //     //Imagen
        //     if(is_file($user->getImagen()))
        //     {
        //         $path = $user->getImagen();
        //     }
        //     else
        //     {
        //         $path = "assets/img/user.png";
        //     }

        //     $type = pathinfo($path, PATHINFO_EXTENSION);
        //     $data = file_get_contents($path);
        //     $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        //     $dataView['IMG_PERFIL'] = $base64;

        //     if($view!="home")
        //     {
        //         $dataView['BURBUJA_CARRITO'] = $this->getTPLtoString("burbuja_carrito");
        //     }
        //     else
        //     {
        //         $dataView['BURBUJA_CARRITO'] = "";
        //     }

        //     $dataView['URLsocket'] = $this->ci->config->item('node_socket')."/socket.io/socket.io.js";
        //     $dataView['URLcarrito'] = base_url()."Resource/js/carritos";

        //     $dataView['URLbusqueda'] = $this->ci->config->item('node_http');

            $templ = $this->ci->parser->parse($view.'.tpl',$dataView,true);
            $dataView['VIEW'] = $templ;

            $this->ci->parser->parse('MasterPage.tpl',$dataView);
        // }
        // else
        // {
        //     redirect('auth/logout', 'refresh');
        // }
    }

    function show_panel($view,$dataView = array()){
        $userSess = $this->ci->session->userdata();
        $user = new Users($userSess['user_id']);

        if($user->getGroup_id()==1){
            $dataView['BASE'] = base_url();
            $dataView['CSS'] = base_url()."Resource/css/";
            $dataView['JS'] = base_url()."Resource/js/";
            $dataView['IMG'] = base_url()."Resource/img/";
            $dataView['APELLIDO'] = $user->getLast_name();
            $dataView['NOMBRE'] = $user->getFirst_name();
            $dataView['FARMACIA'] = $user->getCompany();
            //Imagen
            if(is_file($user->getImagen()))
            {
                $path = $user->getImagen();
            }
            else
            {
                $path = "assets/img/user.png";
            }

            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            $dataView['IMG_PERFIL'] = $base64;
            $dataView['IMG_LOGO'] = base_url()."assets/img/cofaral48.png";

           
            $templ = $this->ci->parser->parse($view.'.tpl',$dataView,true);
            $dataView['VIEW'] = $templ;

            $this->ci->parser->parse('MasterPage_Panel.tpl',$dataView);    
        }
        else{
            redirect('pedidos', 'refresh');
        }        
    }

    public function get_imagen_modal($imagen){
        $type = pathinfo($imagen, PATHINFO_EXTENSION);
        $data = file_get_contents($imagen);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }

    function getTPLtoString($view,$dataView = array()){
        $dataView['BASE'] = base_url();
        $dataView['CSS'] = base_url()."Resource/css/";
        $dataView['JS'] = base_url()."Resource/js/";

        $templ = $this->ci->parser->parse($view.'.tpl',$dataView,true);
        return $templ;
    }

    function getTPLtoPDF($view,$dataView = array()){
        $dataView['BASE'] = base_url();
        $dataView['CSS'] = base_url()."Resource/css/";
        $dataView['JS'] = base_url()."Resource/js/";

        $templ = $this->ci->parser->parse($view.'.tpl',$dataView,true);
        $dataView['VIEW'] = $templ;

        $this->ci->parser->parse('pdf_header.tpl',$dataView);
    }

	public function getUserData(){

        if ($this->ci->ion_auth->logged_in())
        {
            $userSess = $this->ci->session->userdata();
            $user = new Users($userSess['user_id']);
        }
        else
        {
            echo $user = new Users();
        }
      	return $user->convertToJSON();
    }

    public function getCierreZona()
    {
        if ($this->ci->ion_auth->logged_in())
        {
            $userSess = $this->ci->session->userdata();
            $user = new Users($userSess['user_id']);

            $_sql = "SELECT t.listado, t.salida
                        FROM ctacte_clientes c LEFT JOIN ped_turnos t ON (c.zona=t.zona AND c.resp=t.sucursal)
                        WHERE c.codigo=".$user->getCodcli()."
                            AND t.dias LIKE CONCAT('%',DAYOFWEEK(NOW()),'%')
                            AND DATE_FORMAT(NOW(),'%H:%i')<LISTADO
                            AND (t.tipzon='' OR t.tipzon like CONCAT('%-',c.tipzon,'-%'))
                        ORDER BY listado ASC";
            $aRresult = $this->ci->db->query($_sql)->result_array();
            if(count($aRresult)>0)
            {
                return $aRresult[0]["listado"];
            }
            else
            {
                return "--:--";
            }
        }
        else
        {
            return "--:--";
        }
    }

    public function getCreditoDisponible()
    {
        if ($this->ci->ion_auth->logged_in())
        {
            $userSess = $this->ci->session->userdata();
            $user = new Users($userSess['user_id']);

            $_sql = "SELECT c.margen, c.credito, c.credito-c.margen as saldo
                        FROM ctacte_clientes c
                        WHERE c.codigo=".$user->getCodcli().";";
            $aRresult = $this->ci->db->query($_sql)->result_array();
            if(count($aRresult)>0)
            {
                return "$ ".number_format($aRresult[0]["margen"], 2, ',', '.');
            }
            else
            {
                return "$ 00,00";
            }
        }
        else
        {
            return "$ 00,00";
        }
    }

    public function InformarError(Exception $e)
    {
        try
        {
            $this->ci->load->library('ion_auth');
            $this->ci->load->model('ORM/Entities/Users');
            $this->ci->load->library('email');

            $this->ci->email->from('envios.tucuman@cofaral.com.ar', 'COFARAL Ltda.');
            $this->ci->email->to("lbautista@cofaral.com.ar");//$mail_responsable);
            $this->ci->email->cc("jcaniparoli@cofaral.com.ar, csantucho@cofaral.com.ar");
            $this->ci->email->subject("Error en Extranet");

            if ($this->ci->ion_auth->logged_in())
            {
                $userSess = $this->ci->session->userdata();
                $user = new Users($userSess['user_id']);

                $this->ci->email->message(
                    "
                El mensaje de la excepción: ".$e->getmessage()."<br>
                El código de la excepción: ".$e->getcode()."<br>
                El nombre del fichero donde se originó la excepción: ".$e->getfile()."<br>
                La línea donde se originó la excepción: ".$e->getLine()."<br>
                Datos del Usuario: ".$user->convertToJSON()."<br>
            "
                );
            }
            else
            {
                $this->ci->email->message(
                    "
                El mensaje de la excepción: ".$e->getmessage()."<br>
                El código de la excepción: ".$e->getcode()."<br>
                El nombre del fichero donde se originó la excepción: ".$e->getfile()."<br>
                La línea donde se originó la excepción: ".$e->getLine()."<br>
            "
                );
            }

            if ($this->ci->email->send())
            {
                $data = array('status'=>'Error - se ha informado a Cofaral, Intente nuevamente más tarde...');
            }
            else
            {
                $data = array('status'=>'Error - comuniquese con Cofaral o intente màs tarde...');
            }
            return json_encode($data);
        }
        catch(Exception $e)
        {
            $data = array('status'=>'Error - comuniquese con Cofaral o intente màs tarde...');
            return json_encode($data);
        }
    }

    private static $UNIDADES = [
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
    ];

    private static $DECENAS = [
        'VENTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
    ];

    private static $CENTENAS = [
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
    ];

    public static function convertir($number, $moneda = '', $centimos = '')
    {
        $converted = '';
        $decimales = '';

        if (($number < 0) || ($number > 999999999)) {
            return 'No es posible convertir el numero a letras';
        }

        $div_decimales = explode('.',$number);

        if(count($div_decimales) > 1){
            $number = $div_decimales[0];
            $decNumberStr = (string) $div_decimales[1];
            if(strlen($decNumberStr) == 2){
                $decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
                $decCientos = substr($decNumberStrFill, 6);
                $decimales = self::convertGroup($decCientos);
            }
        }

        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);

        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', self::convertGroup($millones));
            }
        }

        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', self::convertGroup($miles));
            }
        }

        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', self::convertGroup($cientos));
            }
        }

        if(empty($decimales)){
            $valor_convertido = $converted . strtoupper($moneda);
        } else {
            $valor_convertido = $converted . strtoupper($moneda) . ' CON ' . $decimales . ' ' . strtoupper($centimos);
        }

        return $valor_convertido;
    }

    private static function convertGroup($n)
    {
        $output = '';

        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = self::$CENTENAS[$n[0] - 1];
        }

        $k = intval(substr($n,1));

        if ($k <= 20) {
            $output .= self::$UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            }
        }

        return $output;
    }

    public function getDigitVerificBarCode($num)
    {
        $vNumeros = str_split($num);
        //paso 1
        $sumImp = 0;
        for($i=0;count($vNumeros)>$i;$i=$i+2)
        {
            $sumImp = $sumImp + (int)$vNumeros[$i];
        }
        //paso 2
        $sumImp = $sumImp*3;
        //paso 3
        $sumPar = 0;
        for($i=1;count($vNumeros)>$i;$i=$i+2)
        {
            $sumPar = $sumPar + (int)$vNumeros[$i];
        }
        // paso 4
        $sumTot = 0;
        $sumTot = $sumImp + $sumPar;
        //paso 5
        $multipo10 = 0;
        while($sumTot>$multipo10)
        {
            $multipo10 = $multipo10+10;
        }
        $digit = $multipo10 - $sumTot;

        return (string)$digit;

    }

    public function verificaPermisos($user,$view)
    {
        $cs = new CollectionWeb_seccion();
        $s = new Web_seccion();
        for($i=0;$cs->count()>$i;$i++)
        {
            $WViews = explode(",",$cs->web_seccion[$i]->getViews());
            if(in_array($view,$WViews))
            {
                $s = $cs->web_seccion[$i];
            }
        }

        if($s->getHabilitada()==0 && $s->getId()!=null)
        {
            $view = "seccion_no_habilitada";
        }
        else
        {
            if($s->getId()!=0)
            {
                if($user->getGroup_id()!=1)
                {
                    $cu = new CollectionUsers("codcli=".$user->getCodcli()." and group_id=2");
                    if($cu->count()>0)
                    {
                        $dcol = new CollectionWeb_denied("
                    (users_id=".$user->getId()." and web_seccion_id=".$s->getId().") OR
                    (users_id=".$cu->users[0]->getId()." and web_seccion_id=".$s->getId().")");

                        if($dcol->count()>0)
                        {
                            $view = "seccion_acceso_denegado";
                        }
                    }
                    else
                    {
                        $view = "seccion_acceso_denegado";
                    }
                }

            }

        }
        return $view;
    }
    
}
